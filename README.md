# Information about the solver
This solver uses the PANM model developed by Puyol, et al. in a biofilm modelling framework. The paper associated with this solver steps the user through the inner workings of the model. This file serves to help the user with the setup.

## Dependencies
 - OpenFOAM v5 from the [OpenFOAM foundation](https://openfoam.org/version/5-0/). We assume that you have this installed without any errors.
 - [photoBio libraries](https://gitlab.com/leboucher/photoBio) for solving the radiative field using the discrete ordinates method (DOM).

It doesn't matter if the OpenFoam distribution is installed in the `/opt` directory or the `$HOME` directory, as the ``wmake`` instructions can handle this.

It is important to set some environment variables in your `~/.bashrc` or `~/.zshrc`, depending on your preference of shell. The directories need to exist in the first place before these environment variables are set. All of the existing environment variables should exist already with the setup of your OpenFOAM installation. The new environment variables are as follows:

``export FOAM_RUN=$WM_PROJECT_USER_DIR/run``

``export FOAM_USER_APP=$WM_PROJECT_USER_DIR/applications``

``export FOAM_USER_SOLVERS=$FOAM_USER_APP/solvers``

``export FOAM_USER_SRC=$WM_PROJECT_USER_DIR/src``