# What to do to finish this work
## Step 0:
Update scalar source terms to only be displayed in the alpha1 phase
Update the flux fields and alpha source terms to be based on VS concentrations. *NO*
Update the rhoMax to reflect the COD concentration

## Step 1:
Change the grid to being a uniform block with a finer dx, adjust dt accordingly
Fixed dt > *DONE*

## Step 2:
Add the radiation terms and update the species equations accordingly


## Step 3:
Use the funky set fields to play with some different initial conditions

## Step 4:
Update the simulation cases to 3D if time permits

